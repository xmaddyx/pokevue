import Pokemon from '../../api/pokemonApi'

const state = {
  all: {},
  types: {},
  subtypes: {},
  supertypes: {},
  filter: {},
  detail: {}
}

const getters = {
  allPokemon: state => state.all,
  detailPokemon: state => state.detail
}

const actions = {
  getAllPokemon ({ commit }) {
    return Pokemon.getAll().then(response => {
      commit('setPokemon', response)
    })
  },
  getTypes ({ commit }) {
    return Pokemon.getType().then(response => {
      commit('setTypes', response)
    })
  },
  getSubtypes ({ commit }) {
    return Pokemon.getSubtype().then(response => {
      commit('setSubtypes', response)
    })
  },
  getSupertypes ({ commit }) {
    return Pokemon.getSupertype().then(response => {
      commit('setSupertypes', response)
    })
  },
  getPokemonByType ({ commit }, filters) {
    return Pokemon.getByFilter(filters).then(response => {
      commit('setFilteredPokemon', response)
    })
  },
  getPokemonById ({ commit }, id) {
    return Pokemon.getById(id).then(response => {
      commit('setdetailedPokemon', response)
    })
  }
}

const mutations = {
  setPokemon (state, list) {
    state.all = list
  },
  setTypes (state, result) {
    state.types = result.data.types
  },
  setSubtypes (state, result) {
    state.subtypes = result.data.subtypes
  },
  setSupertypes (state, result) {
    state.supertypes = result.data.supertypes
  },
  setFilteredPokemon (state, result) {
    state.filter = result.data.cards
  },
  setdetailedPokemon (state, result) {
    state.detail = result.data.card
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
