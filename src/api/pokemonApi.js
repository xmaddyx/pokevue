export default {
  getAll () {
    return new Promise((resolve, reject) => {
      window.axios.get('cards?page=1&pageSize=10').then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getType () {
    return new Promise((resolve, reject) => {
      window.axios.get('types').then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getSubtype () {
    return new Promise((resolve, reject) => {
      window.axios.get('subtypes').then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getSupertype () {
    return new Promise((resolve, reject) => {
      window.axios.get('supertypes').then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getByFilter (filters) {
    return new Promise((resolve, reject) => {
      window.axios.get('cards', {
        params: {
          name: filters.name,
          nationalPokedexNumber: filters.nationalPokedexNumber,
          types: filters.types,
          subtype: filters.subtype,
          supertype: filters.supertype,
          pageSize: 10
        }
      }).then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  },
  getById (id) {
    return new Promise((resolve, reject) => {
      window.axios.get('cards/' + id).then(data => {
        resolve(data)
      }).catch(error => {
        reject(error.response)
      })
    })
  }
}
